====================
Adding New Computers
====================

-------------
Initial Setup
-------------

* Complete Windows Setup without using a Microsoft Account
* The first user on the system (local user) should be fsgadmin using the standard administrator password in the password database
* Name computer using the following methodology:  TELARRAY-###, where ### is a unique 3 digit number matching an asset tag
* Remove all OEM software, such as CyberLink, HP Protect Tools, WinZip, etc
* Install all Windows updates

---------------
Join the Domain
---------------

* Login as fsgadmin
* Navigate to the System control panel
* Join domain 'office.telarrayadvisors.com' using a Domain Administrator account (see password database)
* In Active Directory, move the computer account into the appropriate container

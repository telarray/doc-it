=========================
Mobile Hotspot Management
=========================

----------------------
Adding Additional Data
----------------------

* Verify the hotspot's phone number from the hotspot's `About` screen
* Browse to `ATT Go Phone`_ [#lATTLogin]_
* Login using the phone number of the hotspot with the matching password stored in LastPass under `Shared-IT\\Internet`


.. _ATT Go Phone: https://www.paygonline.com/websc/loginPage.html
.. [#lATTLogin] https://www.paygonline.com/websc/loginPage.html
